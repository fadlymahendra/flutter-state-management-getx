import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:state_management_getx/main.dart';
import 'pages/home_page.dart';
import 'pages/snackbar.dart';
import 'pages/dialog.dart';
import 'pages/bottomsheet.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      // home: HomePage(),
      // home: Snackbar(),
      // home: Dialogx(),
      home: BottomS(),
    );
  }
}
