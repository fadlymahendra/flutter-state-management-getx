import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:state_management_getx/main.dart';

class Snackbar extends StatelessWidget {
  const Snackbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Snackbar GetX"),
      ),
      body: Center(
        child: OutlinedButton(
          child: (Text("Open Snackbar")),
          // child: Snackbar(key: key),
          onPressed: () {
            // ScaffoldMessenger.of(context).showSnackBar(
            //   SnackBar(
            //     content: Text("HALO"),
            //     action: SnackBarAction(
            //       label: "Cancel",
            //       onPressed: () {},
            //     ),
            //   ),
            // );

            // This is using GetX
            Get.snackbar(
              "Hallo",
              "Ini adalah pesannya",
              snackPosition: SnackPosition.BOTTOM,
              backgroundColor: Colors.redAccent,
              showProgressIndicator: true,
            );
          },
        ),
      ),
    );
  }
}
