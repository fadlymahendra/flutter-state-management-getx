import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Dialogx extends StatelessWidget {
  const Dialogx({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dialog GetX"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            // showDialog(
            //   context: context,
            //   builder: (context) => AlertDialog(
            //     shape: RoundedRectangleBorder(
            //       borderRadius: BorderRadius.circular(20),
            //     ),
            //     title: Text("ini Judul"),
            //     content: Text("Ini contennya ya mannteman"),
            //   ),
            // );

            // Using getX
            Get.defaultDialog(
              title: "Ini Judul",
              content: Text("Ini teksnya konten ya mannteman"),
              actions: [
                ElevatedButton(
                  onPressed: () {},
                  child: Text("Cancel"),
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Text("Next"),
                )
              ],
            );

            // Get.dialog(
            //   AlertDialog(
            // //     shape: RoundedRectangleBorder(
            // //       borderRadius: BorderRadius.circular(20),
            // //     ),
            // //     title: Text("ini Judul"),
            // //     content: Text("Ini contennya ya mannteman"),
            // //   ),
            // );
          },
          child: Text("Show Dialog"),
        ),
      ),
    );
  }
}
