import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:state_management_getx/main.dart';

class BottomS extends StatelessWidget {
  const BottomS({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bottom Sheet getX"),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text("Bottom Sheet"),
          onPressed: () {
            Get.bottomSheet(
              Container(
                height: 300,
                color: Colors.amber,
                child: ListView(
                  children: [
                    ListTile(
                      leading: Icon(Icons.settings),
                      title: Text("Setting"),
                    ),
                    ListTile(
                      leading: Icon(Icons.holiday_village),
                      title: Text("Holiday"),
                    ),
                    ListTile(
                      leading: Icon(Icons.mosque),
                      title: Text("Masjid"),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
