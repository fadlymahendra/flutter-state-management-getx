import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:state_management_getx/main.dart';

class HomePage extends StatelessWidget {
  // int count = 0;
  final Controller c = Get.put(Controller());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Belajar State Management GetX"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(
              () => Text(
                "${c.count.value}",
                style: TextStyle(fontSize: 50),
              ),
            ),
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  onPressed: () {
                    c.decrement();
                  },
                  child: Text("-"),
                ),
                ElevatedButton(
                  onPressed: () {
                    c.increment();
                  },
                  child: Text("+"),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class Controller extends GetxController {
  var count = 0.obs;
  increment() => count++;
  decrement() => count--;
}
